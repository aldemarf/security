package main

import (
	//"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	s "strings"
)

func main() {

	var (
		serverUrl, method, endpoint string
		port                        int
		client                      http.Client
	)

	//flag.StringVar(&serverUrl, "url", "localhost", "Destination URL")
	//flag.StringVar(&method, "method", "GET", "HTTP method for the request")
	//flag.IntVar(&port, "port", 80, "Destination port")

	//method = s.ToUpper(method)
	//
	//var req *http.Request
	//switch method {
	//case "POST":
	//	req, err = http.NewRequest(method, serverUrl, body)
	//	if err != nil {
	//		log.Fatal(err)
	//	}
	//default:
	//	req, err = http.NewRequest(method, serverUrl, nil)
	//}
	serverUrl = "http://physecure"
	port = 95
	endpoint = ""

	headers := map[string]string{
		"": "",
	}

	cookies := map[string]string{
		"": "",
	}

	payload := map[string]string{
		"": "",
	}
	body := createBody(payload)

	urlStr := fmt.Sprintf("%s:%d/%s", serverUrl, port, endpoint)
	req, err := http.NewRequest(method, urlStr, body)

	if err != nil {
		log.Fatal(err)
	}

	addHeader(headers, req)
	addCookie(cookies, req)

	res, err := client.Do(req)

	if err != nil {
		log.Fatal(err)
	}

	data, _ := ioutil.ReadAll(res.Body)

	fmt.Println(data)

}

func addHeader(m map[string]string, r *http.Request) {
	for k, v := range m {
		r.Header.Add(k, v)
	}
}

func createBody(m map[string]string) (body io.Reader) {
	if len(m) == 1 && m[""] == "" {
		return nil
	}

	form := url.Values{}
	for k, v := range m {
		form.Add(k, v)
	}
	return s.NewReader(form.Encode())
}

func addCookie(m map[string]string, r *http.Request) {
	for k, v := range m {
		r.AddCookie(&http.Cookie{
			Name:  k,
			Value: v,
		})
	}
}
